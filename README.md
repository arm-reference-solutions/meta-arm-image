Introduction
------------
This repository contains meta-arm-image layer for OpenEmbedded

meta-arm-image:
	This layer provides a common image shared between Corstone1000 Arm platforms.
	The image is called corstone1000-image.
	Each platform can extend corstone1000-image by including inc files to provide
	custom image settings tailored to the selected machine.

For more information please contact the maintainers.

Maintainer(s)
-------------
* Jon Mason <jon.mason@arm.com>
* Rui Miguel Silva <rui.silva@arm.com>
* Abdellatif El Khlifi <abdellatif.elkhlifi@arm.com>
