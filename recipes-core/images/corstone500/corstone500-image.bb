SUMARY = "Corstone500 platform Image"
DESCRIPTION = "This is the main image which is the container of all the binaries \
               generated for the Corstone500 platform."
LICENSE = "MIT"

inherit core-image
inherit image-buildinfo
inherit wic_nopt

# deleting the /usr/lib/opkg/alternatives folder
# because we do not need alternative commands to what
# busybox offers. This saves around 1MB flash
remove_alternative_files () {
    rm -rf ${IMAGE_ROOTFS}/usr/lib/opkg
}
ROOTFS_POSTPROCESS_COMMAND += "remove_alternative_files;"

# package management is not supported in corstone500
IMAGE_FEATURES:remove = "package-management"

IMAGE_FEATURES += "debug-tweaks"

IMAGE_FSTYPES += "squashfs wic wic.nopt"

#
# images inheriting image.bbclass are forced to have do_populate_sysroot
# dependency from the recipes listed under EXTRA_IMAGEDEPENDS initramfs image
# doesn't need to use sysroot data provided by EXTRA_IMAGEDEPENDS recipes The
# code below sets the right dependency of do_image_complete task This prevents
# dependency loops
#
python () {
    if d.getVar('IMAGE_BASENAME') == "corstone500-image":
        d.setVarFlag('do_image_complete', 'depends', 'corstone500-image:do_image')
}
