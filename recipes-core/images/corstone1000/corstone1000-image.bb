SUMARY = "Corstone1000 platform Image"
DESCRIPTION = "This is the main image which is the container of all the binaries \
               generated for the Corstone1000 platform."
LICENSE = "MIT"

inherit core-image
inherit wic_nopt

IMAGE_FEATURES += "debug-tweaks"

IMAGE_FSTYPES += "wic wic.nopt"
