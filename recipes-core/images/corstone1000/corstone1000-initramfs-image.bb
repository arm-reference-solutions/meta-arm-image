SUMARY = "Corstone1000 platform Initramfs Image"
DESCRIPTION = "This is the main Linux image which includes an initramfs kernel/rootfs bundle."

LICENSE = "MIT"

IMAGE_FSTYPES = "${INITRAMFS_FSTYPES}"

inherit core-image

# By default all basic packages required for a bootable system are installed
# by core-image . These packages are: packagegroup-core-boot and
# packagegroup-base-extended

inherit image-buildinfo

IMAGE_FEATURES += "debug-tweaks"

# deleting the /usr/lib/opkg/alternatives folder
# because we do not need alternative commands to what
# busybox offers. This saves around 1MB flash
remove_alternative_files () {
    rm -rf ${IMAGE_ROOTFS}/usr/lib/opkg
}
ROOTFS_POSTPROCESS_COMMAND += "remove_alternative_files;"

#package management is not supported in corstone1000
IMAGE_FEATURES:remove = "package-management"

# all optee packages
IMAGE_INSTALL += "optee-client"

# FF-A Debugfs driver
IMAGE_INSTALL += "ffa-debugfs-mod"

# psa-arch-tests linux userspace application
IMAGE_INSTALL += "secure-partitions-psa-api-tests"

#
# images inheriting image.bbclass are forced to have do_populate_sysroot dependency from the recipes 
# listed under EXTRA_IMAGEDEPENDS
# initramfs image doesn't need to use sysroot data provided by EXTRA_IMAGEDEPENDS recipes
# The code below sets the right dependency of do_image_complete task
# This prevents dependency loops
#
python () {
    if d.getVar('IMAGE_BASENAME') == "corstone1000-initramfs-image":
        d.setVarFlag('do_image_complete', 'depends', 'corstone1000-initramfs-image:do_image')
}
