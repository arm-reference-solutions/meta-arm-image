SUMARY = "Corstone1000 platform Image"
DESCRIPTION = "This is the main image which is the container of all the binaries \
               generated for the Corstone1000 platform."
LICENSE = "MIT"

inherit image
inherit wic_nopt

COMPATIBLE_MACHINE:corstone1000 = "corstone1000"

PREFERRED_PROVIDER_virtual/kernel:forcevariable = "linux-dummy"

python() {
    if d.getVar("TFA_UBOOT") != '0':
        raise bb.parse.SkipRecipe("TFA_UBOOT must be '0' to proceed or run kas shell meta-arm/kas/corstone1000-fvp.yml:meta-arm/kas/tftf.yml")
}

WKS_FILE = "corstone1000-fipimage-tftf.wks"

PACKAGE_INSTALL=""

IMAGE_FSTYPES += "wic wic.nopt"
